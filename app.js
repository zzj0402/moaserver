var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var datasetRouter = require("./routes/dataset");
var moaRouter = require("./routes/moa");
var reportRouter = require("./routes/report");
var historyRouter = require("./routes/history");
var userRouter = require("./routes/user");
var cors = require("cors");
var passport = require("passport");

var app = express();

app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(
  express.urlencoded({
    extended: false
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.use("/", indexRouter);
app.use("/dataset", datasetRouter);
app.use("/moa", moaRouter);
app.use("/report", reportRouter);
app.use("/history", historyRouter);
app.use("/user", userRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});
const port = 2025;
app.listen(port, () =>
  console.log(`MOA server listening on port http://localhost:${port} !`)
);
module.exports = app;
