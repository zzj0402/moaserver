![coverage](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)
[![Gitter](https://badges.gitter.im/Waikato-Univeristy/MOA.svg)](https://gitter.im/Waikato-Univeristy/MOA?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
# MOA server

## Prerequisite

- Docker Compose

## Setup
### Server
`docker-compose up --build`
### Client
`ssh -L 2025:localhost:2025 YOUR_USER_NAME@YOUR_SERVER_ADDRESS` tunnel local port 2025