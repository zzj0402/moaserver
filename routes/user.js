var express = require("express");
var router = express.Router();

var passport = require("passport");
var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
const GoogleSecretObject = require("../secret.json");
var MongoClient = require("mongodb").MongoClient;

// Use the GoogleStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and Google
//   profile), and invoke a callback with a user object.
passport.use(
  new GoogleStrategy(
    {
      clientID: GoogleSecretObject.web.client_id,
      clientSecret: GoogleSecretObject.web.client_secret,
      callbackURL: "/user/auth/google/callback"
    },
    function(accessToken, refreshToken, profile, done) {
      console.log(profile);
      // User.findOrCreate({ googleId: profile.id }, function(err, user) {
      //   return done(err, user);
      // });
      MongoClient.connect("mongodb://db:27017/MOA", function(err, db) {
        if (err) throw err;
        var dbo = db.db("MOA");
        dbo
          .collection("users")
          .findOne({ sub: profile.id }, function(err, res) {
            console.log("Result of finding users:" + res);
            if (err) throw err;
            if (!res) {
              console.log("user of profile id " + profile.id + " not found!");
              dbo
                .collection("users")
                .insertOne(profile._json, function(err, res) {
                  if (err) throw err;
                  console.log(profile.id + " user created!");
                  return done(err, true);
                });
            } else {
              //found user. Return
              console.log(profile.id + " user found!");
              console.log(res);
              return done(err, true);
            }
            db.close();
          });
        dbo
          .collection("users")
          .find({})
          .toArray(function(err, result) {
            if (err) throw err;
            console.log(result);
            db.close();
          });
      });
    }
  )
);

/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("index", {
    title: "User Authentication"
  });
});

// GET /auth/google
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in Google authentication will involve
//   redirecting the user to google.com.  After authorization, Google
//   will redirect the user back to this application at /auth/google/callback
router.get(
  "/auth/google",
  passport.authenticate("google", {
    scope: ["https://www.googleapis.com/auth/userinfo.email"]
  })
);

// GET /auth/google/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
router.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    session: false,
    successRedirect: "http://localhost:4200/setting",
    failureRedirect: "/user/login"
  })
);

/* Login information needed */
router.get("/login", function(req, res, next) {
  res.render("index", {
    title: "User Authentication failed, please contact administrator!"
  });
});

/* Return user ID on post request*/
router.post("/", function(req, res, next) {
  MongoClient.connect("mongodb://db:27017/MOA", function(err, db) {
    if (err) throw err;
    var dbo = db.db("MOA");
    dbo
      .collection("users")
      .find({ email: req.body.email })
      .toArray(function(err, result) {
        if (err) throw err;
        console.log(result);
        if (result.length == 0) {
          res.send(428);
        } else {
          if (result[0].sub) {
            res.status(200).send(result[0]);
          } else {
            res.status(428).send("User not found!.");
          }
        }
        db.close();
      });
  });
});

module.exports = router;
