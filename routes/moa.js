var express = require("express");
var router = express.Router();
const spawn = require("child_process").spawn;
const os = require("os");
const uuidv1 = require("uuid/v1");
const fs = require("fs");
var MongoClient = require("mongodb").MongoClient;
var Code = require("mongodb").Code;
var url = "mongodb://db:27017/MOA";
var moaReport = "";
async function DoTask(taskUUID, userID, parameters) {
  // Takes parameters for moa.DoTask
  // MOA task parameter string. eg:"EvaluatePrequential -l (meta.OzaBag -l (trees.HoeffdingTree -g 50)) -s generators.AgrawalGenerator -i 100000 -f 1000"
  // Report string
  var child;
  var moaDoTaskCommands = "java -cp ./lib/moa.jar moa.DoTask ";
  var modifiedParameters = parameters.replace(
    "(ArffFileStream -f ",
    "(ArffFileStream -f datasets/" + userID + "_"
  );
  moaDoTaskCommands +=
    '"' + modifiedParameters + '"' + " -d " + taskUUID + ".dump";
  if (os.type() == "Windows_NT") {
    child = spawn("cmd.exe", [
      "/c",
      moaDoTaskCommands + " >>" + taskUUID + ".out" + " 2>" + taskUUID + ".err"
    ]);
  }
  if (os.type() == "Linux") {
    child = spawn("/bin/sh", [
      "-c",
      moaDoTaskCommands + " >>" + taskUUID + ".out" + " 2>" + taskUUID + ".err"
    ]);
  }
  fs.watchFile(taskUUID + ".err", (curr, prev) => {
    fs.exists(taskUUID + ".err", exists => {
      if (exists) {
        fs.readFile(taskUUID + ".err", "utf8", (err, stderr) => {
          if (err) throw err;
          console.log("reading of " + taskUUID + ".err: " + stderr);
          MongoClient.connect(url, function(err, db) {
            if (err) throw err;
            var dbo = db.db("MOA");
            dbo.collection("tasks").updateOne(
              {
                taskID: taskUUID
              },
              {
                $set: {
                  lastUpdate: new Date().toISOString(),
                  status: Code(stderr)
                }
              }
            );
            db.close();
          });
        });
      }
    });
  });

  fs.watchFile(taskUUID + ".dump", (curr, prev) => {
    fs.exists(taskUUID + ".dump", exists => {
      if (exists) {
        fs.readFile(taskUUID + ".dump", "utf8", (err, stddump) => {
          if (err) throw err;
          console.log("reading of " + taskUUID + ".dump: " + stddump);
          MongoClient.connect(url, function(err, db) {
            if (err) throw err;
            var dbo = db.db("MOA");
            var stddumpCodified = Code(stddump);
            dbo.collection("tasks").updateOne(
              {
                taskID: taskUUID
              },
              {
                $set: {
                  lastUpdate: new Date().toISOString(),
                  report: stddumpCodified
                }
              }
            );
            db.close();
          });
        });
      }
    });
  });

  fs.watchFile(taskUUID + ".out", (curr, prev) => {
    fs.exists(taskUUID + ".out", exists => {
      if (exists) {
        fs.readFile(taskUUID + ".out", "utf8", (err, stdout) => {
          if (err) throw err;
          console.log("reading of " + taskUUID + ".out: " + stdout);
          MongoClient.connect(url, function(err, db) {
            if (err) throw err;
            var dbo = db.db("MOA");
            var stdoutCodified = Code(stdout);
            dbo.collection("tasks").updateOne(
              {
                taskID: taskUUID
              },
              {
                $set: {
                  lastUpdate: new Date().toISOString(),
                  report: stdoutCodified
                }
              }
            );
            db.close();
          });
        });
      }
    });
  });

  //Update on status
  child.stderr.on("data", data => {
    console.log(`stderr: ${data}`);
  });
  //Update on report
  child.stdout.on("data", data => {
    console.log(`stdout: ${data}`);
    moaReport += data;
  });

  child.on("close", code => {
    console.log(`child process exited with code ${code}`);
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db("MOA");
      dbo.collection("tasks").updateOne(
        {
          taskID: taskUUID
        },
        {
          $set: {
            lastUpdate: new Date().toISOString(),
            status: "100%"
          }
        }
      );
      db.close();
    });
  });
  return moaReport;
}

router.post("/", async function(req, res, next) {
  var taskUUID = uuidv1();
  // console.log(taskUUID);
  var taskOutput = await DoTask(taskUUID, req.body.userID, req.body.task);
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("MOA");
    var task = {
      userID: req.body.userID,
      taskID: taskUUID,
      command: req.body.task,
      startTime: new Date().toISOString(),
      lastUpdate: new Date().toISOString(),
      status: null,
      report: null
    };
    dbo.collection("tasks").insertOne(task, function(err, res) {
      if (err) throw err;
      console.log(task.taskID + " task created!");
    });
    db.close();
    res.status(202).send(task);
  });
});

module.exports = router;
