var express = require("express");
var router = express.Router();
var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://db:27017/MOA";
// MongoClient.connect(url, function(err, db) {
//   if (err) throw err;
//   console.log("Database connected!");
//   db.close();
// });

/* Handles request for report */
router.post("/", function(req, res, next) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("MOA");
    dbo
      .collection("tasks")
      .find({
        userID: req.body.userID
      })
      .toArray(function(err, tasks) {
        if (err) throw err;
        if (!tasks) {
          res.send(404);
        } else {
          res.status(200).send(tasks);
        }
        db.close();
      });
  });
  // fs.readFile('./reports/' + req.body.taskUUID + '.report', 'utf8', function (err, contents) {
  //     res.status(200).send(contents);
  //     console.log(req.body.taskUUID + ' read!');
  // });
});

module.exports = router;
