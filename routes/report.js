var express = require("express");
var router = express.Router();
var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://db:27017/MOA";
// MongoClient.connect(url, function(err, db) {
//   if (err) throw err;
//   console.log("Database connected!");
//   db.close();
// });

/* Handles request for report */
router.post("/", function(req, res, next) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("MOA");
    dbo.collection("tasks").findOne(
      {
        taskID: req.body.taskUUID
      },
      function(err, result) {
        if (err) throw err;
        if (!result) {
          res.send(404);
        } else {
          if (result.report) {
            var response = {
              command: result.command,
              startTime: result.startTime,
              lastUpdate: result.lastUpdate,
              status: result.status,
              report: result.report.code
            };
            res.status(200).send(response);
          } else {
            if (result.status) {
              var response = {
                startTime: result.startTime,
                lastUpdate: result.lastUpdate,
                command: result.command,
                status: result.status.code
              };
              res.status(200).send(response);
            } else {
              return res
                .status(428)
                .send("Neither report or status is generated.");
            }
          }
          db.close();
        }
      }
    );
  });
  // fs.readFile('./reports/' + req.body.taskUUID + '.report', 'utf8', function (err, contents) {
  //     res.status(200).send(contents);
  //     console.log(req.body.taskUUID + ' read!');
  // });
});

module.exports = router;
