var express = require("express");
var router = express.Router();
var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://db:27017/MOA";
// MongoClient.connect(url, function(err, db) {
//   if (err) throw err;
//   console.log("Database connected!");
//   db.close();
// });

/* Handles request for dataset */
router.post("/", function(req, res, next) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("MOA");
    dbo
      .collection("users")
      .find({
        userID: req.body.userID
      })
      .toArray(function(err, users) {
        if (err) throw err;
        if (!users) {
          res.send(404);
        } else {
          res.status(200).send(users[0]);
        }
        db.close();
      });
  });
  // fs.readFile('./reports/' + req.body.taskUUID + '.report', 'utf8', function (err, contents) {
  //     res.status(200).send(contents);
  //     console.log(req.body.taskUUID + ' read!');
  // });
});

module.exports = router;
