var express = require("express");
var router = express.Router();
var multer = require("multer");
var path = require("path");
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "datasets");
  },
  filename: function(req, file, cb) {
    cb(null, req.body.userID + "_" + file.originalname);
  }
});
var upload = multer({
  storage: storage
});
var MongoClient = require("mongodb").MongoClient;
var router = express.Router();

router.post("/", upload.single("dataset"), function(req, res, next) {
  const file = req.file;
  const userID = req.body.userID;
  const md5 = req.body.md5;
  console.log(md5);
  if (!file) {
    const error = new Error("Please upload a valid arff file");
    error.httpStatusCode = 400;
    return next(error);
  } else {
    var extension = path.extname(file.originalname);
    // console.log(extension);
    if (extension != ".arff" && extension != ".ARFF") {
      const error = new Error("Please upload a valid arff file");
      error.httpStatusCode = 400;
      return next(error);
    } else {
      console.log("Dataset accepted!");
      // Check for md5 duplicates
      MongoClient.connect("mongodb://db:27017/MOA", function(err, db) {
        if (err) throw err;
        var dbo = db.db("MOA");
        dbo
          .collection("md5")
          .find({
            md5: md5
          })
          .toArray(function(err, documents) {
            if (err) throw err;
            if (documents === undefined || documents.length == 0) {
              console.log("New file, no duplicates");
              dbo.collection("md5").insertOne({ md5: md5 }, function(err, res) {
                if (err) throw err;
                console.log(md5 + "file md5 inserted!");
              });
              dbo
                .collection("users")
                .find({
                  userID: userID
                })
                .toArray(function(err, users) {
                  if (err) throw err;
                  if (users === undefined || users.length == 0) {
                    console.log("New user!");
                    dbo
                      .collection("users")
                      .insertOne({ userID: userID, datasets: [] }, function(
                        err,
                        res
                      ) {
                        if (err) throw err;
                        console.log(userID + " user created!");
                      });
                  }
                  dbo.collection("users").updateOne(
                    { userID: userID },
                    {
                      $push: {
                        datasets: {
                          Name: file.originalname,
                          CreationTime: new Date().toISOString(),
                          UploadedBy: userID,
                          Size: file.size + " bytes"
                        }
                      }
                    },
                    function(error, result) {
                      if (error) {
                        throw error;
                      }
                      dbo
                        .collection("users")
                        .find({
                          userID: userID
                        })
                        .toArray(function(err, users) {
                          if (err) throw err;
                          res.send(users[0]);
                        });
                    }
                  );
                });
            } else {
              console.log("Duplicate file with MD5:" + md5);
              dbo
                .collection("users")
                .find({
                  userID: userID
                })
                .toArray(function(err, users) {
                  if (err) throw err;
                  if (users === undefined || users.length == 0) {
                    console.log("New user reuploading!");
                    dbo
                      .collection("users")
                      .insertOne({ userID: userID, datasets: [] }, function(
                        err,
                        res
                      ) {
                        if (err) throw err;
                        console.log(userID + " user created!");
                      });
                    dbo.collection("users").updateOne(
                      { userID: userID },
                      {
                        $push: {
                          datasets: {
                            Name: file.originalname,
                            CreationTime: new Date().toISOString(),
                            UploadedBy: userID,
                            Size: file.size + " bytes"
                          }
                        }
                      },
                      function(error, result) {
                        if (error) {
                          throw error;
                        }
                        dbo
                          .collection("users")
                          .find({
                            userID: userID
                          })
                          .toArray(function(err, users) {
                            if (err) throw err;
                            res.send(users[0]);
                          });
                      }
                    );
                  } else {
                    //Old user, file uploaded, do not update
                    dbo
                      .collection("users")
                      .find({
                        userID: userID
                      })
                      .toArray(function(err, users) {
                        if (err) throw err;
                        res.send(users[0]);
                      });
                  }
                });
            }
          });
        // db.close();
      });
    }
  }
});
/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("index", {
    title: "MOA server"
  });
});

module.exports = router;
