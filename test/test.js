const request = require("supertest");
const app = require("../app");
var mongodb = require("mongo-mock");
mongodb.max_delay = 0; //you can choose to NOT pretend to be async (default is 400ms)
var MongoClient = mongodb.MongoClient;
// Connection URL
var url = "mongodb://localhost:27017/MOA";
// Use connect method to connect to the Server
var taskUUID = "9a78feb0-a91c-11e9-9b33-fd9fe746df19";
MongoClient.connect(url, {}, function(err, client) {
  if (err) return err;
  var db = client.db();
  // Get the documents collection
  var collection = db.collection("tasks");
  console.log("Database connected");
});
const GoogleSecretObject = require("../secret.json");
const assert = require("assert");
describe("App", function() {
  it("has the default page", function(done) {
    request(app)
      .get("/")
      .expect(/MOA/, done);
  });
});

describe("Authentication Module", function() {
  it("has the default page", function(done) {
    request(app)
      .get("/user")
      .expect(200, done);
  });

  it("verifies credentials", function(done) {
    assert(GoogleSecretObject.web.client_id, "Client ID not valid");
    assert(GoogleSecretObject.web.client_secret, "Client Secret not valid");
    done();
  });

  it("gets authentication redirection", function(done) {
    request(app)
      .get("/user/auth/google/callback")
      .expect(302, done);
  });
  it("redirects to landing page when authentication fails", function(done) {
    request(app)
      .get("/user/login")
      .expect(200, done);
  });
  it("handles client Sign-In request", function(done) {
    request(app)
      .get("/user/auth/google/")
      .expect(302, done);
  });
});
