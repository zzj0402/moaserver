FROM waikato/moa
COPY . /app/moa
WORKDIR /app/moa
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs
RUN npm install
CMD [ "npm", "start" ]